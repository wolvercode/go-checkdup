package app

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/spf13/viper"
)

// App : Struct of App package
type App struct {
	Configs    *ConfigStruct
	RestAPIURL string
	DBAPIURL   string
}

// Appl : Global variable of App
var Appl App

var ctx = context.Background()

// Initialize : Function for initializing App
func Initialize() {
	Appl.InitConfig()
	Appl.SetRestAPIURL()
	Appl.SetDBAPIURL()
	fmt.Println("....Starting " + Appl.Configs.Main.DisplayName + "....")
}

// Sleeping : Function for sleeping
func Sleeping() {
	fmt.Println("....Sleep for", Appl.Configs.Main.Sleep, "seconds....")
	time.Sleep(time.Duration(Appl.Configs.Main.Sleep) * time.Second)
}

// InitConfig : Function for initializing App Config
func (a App) InitConfig() {
	viper.AddConfigPath("./")
	viper.SetConfigName("go-checkdup")
	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}

	var conf Config
	err := viper.Unmarshal(&conf)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}

	if conf.Active == "dev" {
		Appl.Configs = &conf.Dev
	} else {
		Appl.Configs = &conf.Prod
	}
}

// SetRestAPIURL :
func (a App) SetRestAPIURL() {
	Appl.RestAPIURL = fmt.Sprintf("http://%s:%d/%s/", Appl.Configs.RestAPI.Host, Appl.Configs.RestAPI.Port, Appl.Configs.RestAPI.URL)
}

// SetDBAPIURL :
func (a App) SetDBAPIURL() {
	Appl.DBAPIURL = fmt.Sprintf("http://%s:%d/%s/", Appl.Configs.DbAPI.Host, Appl.Configs.DbAPI.Port, Appl.Configs.DbAPI.URL)
}
