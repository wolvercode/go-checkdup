package handler

import (
	"fmt"

	"bitbucket.org/billing/go-checkdup/model"
)

// Process :
type Process struct {
	ProcessStatusID     string `json:"processstatus_id"`
	MinStartcall        string `json:"min_startcall"`
	MaxStartcall        string `json:"max_startcall"`
	TotalInput          string `json:"total_input"`
	TotalReject         string `json:"total_reject"`
	TotalDuplicate      string `json:"total_duplicate"`
	TotalUnduplicate    string `json:"total_unduplicate"`
	UnduplicateDuration string `json:"unduplicate_duration"`
	DuplicateDuration   string `json:"duplicate_duration"`
	ErrorMessage        string `json:"error_message"`
}

var modelProcess model.ProcessCheckdup

// GetProcess :
func (p *Process) GetProcess() ([]model.ProcessCheckdup, error) {
	listProcess, err := modelProcess.GetProcess()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return listProcess, nil
}

// UpdateStatus :
func (p *Process) UpdateStatus(processID, processStatusID string) error {
	err := modelProcess.UpdateStatus(processID, processStatusID)
	if err != nil {
		fmt.Println("  > [ Update Status Failed ] :", err.Error())
		return err
	}
	return nil
}

// CloseProcess :
func (p *Process) CloseProcess(closingStruct *model.ClosingStruct) error {
	fmt.Println("  > [ Closing Process With Process StatusID ] :", closingStruct.ProcessStatusID)
	err := modelProcess.CloseProcess(closingStruct)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	return nil
}

// GetCheckDupConf :
func (p *Process) GetCheckDupConf() (model.CheckDupConf, error) {
	output, err := modelProcess.GetCheckDupConf()
	if err != nil {
		fmt.Println(err.Error())
		return model.CheckDupConf{}, err
	}
	return output, nil
}

// GetSlicedProcess :
func (p *Process) GetSlicedProcess(minStartcall, maxStartcall, dataTypeID, moMt, postPre string) ([]model.SlicedProcess, error) {
	output, err := modelProcess.GetSlicedProcess(minStartcall, maxStartcall, dataTypeID, moMt, postPre)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return output, nil
}

// GetDataStructure :
func (p *Process) GetDataStructure() (model.DataStructure, error) {
	output, err := modelProcess.GetDataStructure()
	if err != nil {
		fmt.Println(err.Error())
		return output, err
	}
	return output, nil
}
