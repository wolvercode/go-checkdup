package handler

import (
	"fmt"

	"bitbucket.org/billing/go-checkdup/model"
	"github.com/syndtr/goleveldb/leveldb"
)

// CommonHandler :
type CommonHandler struct {
	DBConn *leveldb.DB
}

// CombineDB :
func (p *CommonHandler) CombineDB(listPathDB *[]model.SlicedProcess, currentDBPath string) error {
	fmt.Printf("  > [ Combining Key For Check Duplicate ] : ")
	var err error
	p.DBConn, err = leveldb.OpenFile(currentDBPath, nil)
	if err != nil {
		fmt.Println("Failed To Open LevelDB, with error =>", err.Error())
		return err
	}

	counts := 0
	for _, v := range *listPathDB {
		subDbConn, err := leveldb.OpenFile(v.PathDB, nil)
		if err != nil {
			fmt.Println("Failed To Open LevelDB, with error =>", err.Error())
			continue
		}
		iter := subDbConn.NewIterator(nil, nil)
		for iter.Next() {
			counts++
			err = p.DBConn.Put([]byte(iter.Key()), []byte("1"), nil)
		}
		iter.Release()

		subDbConn.Close()
	}
	fmt.Println("Success, Total Key =>", counts)

	p.DBConn.Close()

	return nil
}
