package handler

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"

	"bitbucket.org/billing/go-checkdup/model"
	"github.com/syndtr/goleveldb/leveldb"
)

// OCS :
type OCS struct {
	ListProcess     *model.ProcessCheckdup
	CheckDupConf    model.CheckDupParameter
	MinStartcall    string
	MaxStartcall    string
	DBConn          *leveldb.DB
	DataStructure   *model.DataStructure
	totalReject     int
	totalUndup      int
	totalDup        int
	totalUndupDur   int
	totalDupDur     int
	mapStartCall    map[int]int
	urutanStartcall int
	urutanDurasi    int
	urutanOprID     int
	containsParam   []string
	urutanContains  int
	mapCount        map[string]map[string]int
}

// LogStartcall :
func (p *OCS) LogStartcall() (int, error) {
	countInput := 0
	fmt.Println("[ Making Log Startcall & Get Min-Max Startcall Data OCS ]")

	// Opening File CDR
	inputCDR := p.ListProcess.PathCdr + p.ListProcess.FilenameCdr

	fmt.Printf("  > [ Opening Input File ] : " + inputCDR + " => ")
	fileCDR, err := os.OpenFile(inputCDR, os.O_RDONLY, os.ModePerm)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return countInput, err
	}
	fmt.Println("Success")
	defer fileCDR.Close()

	sc := bufio.NewScanner(fileCDR)
	mapStartCall := make(map[int]int)
	urutanStartcall, _ := strconv.Atoi(p.CheckDupConf.ParameterToleransi[0])
	for sc.Scan() {
		countInput++
		columns := strings.Split(strings.Replace(sc.Text(), "+", "", -1), "|")
		stcall, _ := strconv.Atoi(columns[urutanStartcall])
		mapStartCall[stcall] = 1
	}

	// Sorting startcall to get min and max startcall
	var keys []int
	for k := range mapStartCall {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	p.MinStartcall, p.MaxStartcall = strconv.Itoa(keys[0]), strconv.Itoa(keys[len(keys)-1])

	inputLog := inputCDR + ".log"

	filestcall, err := os.Create(inputLog)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return countInput, err
	}
	defer filestcall.Close()

	fmt.Printf("  > [ Writing To Log File ] : " + inputLog + " => ")

	writerStcall := bufio.NewWriter(filestcall)
	fmt.Fprintln(writerStcall, "[ Log Startcall of file : "+inputCDR+" ]")
	for _, v := range keys {
		fmt.Fprintln(writerStcall, v)
	}
	fmt.Println("Success")

	// File reject bufio writer flush
	err = writerStcall.Flush()
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return countInput, err
	}

	return countInput, nil
}

// ProcessData :
func (p *OCS) ProcessData(closingStruct *model.ClosingStruct, currentDBPath string) error {
	fmt.Println("[ Processing Data OCS ]")

	// hProcess := Process{TotalInput: "0", TotalDuplicate: "0", TotalReject: "0", TotalUnduplicate: "0", MinStartcall: "0", MaxStartcall: "0"}

	p.mapCount = make(map[string]map[string]int)

	var err error
	p.DBConn, err = leveldb.OpenFile(currentDBPath, nil)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return err
	}

	// Opening File CDR
	inputCDR := p.ListProcess.PathCdr + p.ListProcess.FilenameCdr

	fmt.Printf("  > [ Opening Input File ] : " + inputCDR + " => ")
	fileCDR, err := os.OpenFile(inputCDR, os.O_RDONLY, os.ModePerm)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return err
	}
	fmt.Println("Success")
	defer fileCDR.Close()

	// Dup intitialization, making directory & file dup bufio writer
	if _, err := os.Stat(p.ListProcess.PathDup); os.IsNotExist(err) {
		fmt.Printf("  > [ Making Dup Directory ] : ")
		err = os.MkdirAll(p.ListProcess.PathDup, 0777)
		if err != nil {
			fmt.Println("Failed, with error =>", err.Error())
			return err
		}
		fmt.Println("Success")
	}

	inputDUP := p.ListProcess.PathDup + p.ListProcess.FilenameDup

	fileDUP, err := os.Create(inputDUP)
	if err != nil {
		log.Fatal(err)
	}
	defer fileDUP.Close()

	writerDUP := bufio.NewWriter(fileDUP)

	// Reject intitialization, making directory & file teject bufio writer
	if _, err := os.Stat(p.ListProcess.PathReject); os.IsNotExist(err) {
		fmt.Printf("  > [ Making Reject Directory ] : ")
		err = os.MkdirAll(p.ListProcess.PathReject, 0777)
		if err != nil {
			fmt.Println("Failed, with error =>", err.Error())
			return err
		}
		fmt.Println("Success")
	}

	inputReject := p.ListProcess.PathReject + p.ListProcess.FilenameReject

	fileReject, err := os.Create(inputReject)
	if err != nil {
		log.Fatal(err)
	}
	defer fileReject.Close()

	writerReject := bufio.NewWriter(fileReject)

	// Undup intitialization, making directory & file undup bufio writer
	if _, err := os.Stat(p.ListProcess.PathUndup); os.IsNotExist(err) {
		fmt.Printf("  > [ Making Undup Directory ] : ")
		err = os.MkdirAll(p.ListProcess.PathUndup, 0777)
		if err != nil {
			fmt.Println("Failed, with error =>", err.Error())
			return err
		}
		fmt.Println("Success")
	}

	inputUndup := p.ListProcess.PathUndup + p.ListProcess.FilenameUndup

	fileUndup, err := os.Create(inputUndup)
	if err != nil {
		log.Fatal(err)
	}
	defer fileUndup.Close()

	writerUndup := bufio.NewWriter(fileUndup)

	// Database initialization, makin directory & opening Connection
	if _, err := os.Stat(p.ListProcess.PathDB); os.IsNotExist(err) {
		fmt.Printf("  > [ Making DB Directory ] : ")
		err = os.MkdirAll(p.ListProcess.PathDB, 0777)
		if err != nil {
			fmt.Println("Failed, with error =>", err.Error())
			return err
		}
		fmt.Println("Success")
	}

	fmt.Printf("  > [ Opening Database ] : ")
	dbconn, err := leveldb.OpenFile(p.ListProcess.PathDB, nil)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return err
	}
	fmt.Println("Success")
	defer dbconn.Close()

	// Processing check Duplicate of CDR File
	fmt.Printf("  > [ Doing Check Duplicate ] : ")
	p.mapStartCall = make(map[int]int)
	p.urutanStartcall, _ = strconv.Atoi(p.CheckDupConf.ParameterToleransi[0])
	p.urutanDurasi = p.DataStructure.TypeData["OCS"].Parameter["CALLDURATION"].Urutan
	p.containsParam = strings.Split(p.CheckDupConf.ParameterContains, "=")
	p.urutanContains = p.DataStructure.TypeData["OCS"].Parameter[p.containsParam[0]].Urutan
	p.urutanOprID = p.DataStructure.TypeData["OCS"].Parameter["TAP_CODE"].Urutan

	sc := bufio.NewScanner(fileCDR)
	for sc.Scan() {
		p.lineProcessing(dbconn, sc, writerDUP, writerReject, writerUndup)
	}
	if err := sc.Err(); err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return err
	}

	// File undup bufio writer flush
	err = writerUndup.Flush()
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return err
	}

	// File dup bufio writer flush
	err = writerDUP.Flush()
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return err
	}

	// File reject bufio writer flush
	err = writerReject.Flush()
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return err
	}

	closingStruct.TotalInput = p.totalReject + p.totalDup + p.totalUndup
	closingStruct.TotalReject = p.totalReject
	closingStruct.TotalDuplicate = p.totalDup
	closingStruct.TotalUnduplicate = p.totalUndup
	closingStruct.UnduplicateDuration = p.totalUndupDur
	closingStruct.DuplicateDuration = p.totalDupDur
	closingStruct.MapCount = p.mapCount

	if p.totalUndup == 0 {
		fmt.Println("Success but no unduplicate record => [ Rejected", p.totalReject, ", Dup", p.totalDup, ", Undup", p.totalUndup, "records, Dup Dur", p.totalDupDur, "seconds ]")
		err := errors.New("Error : No Unduplicate Record")
		return err
	}

	// Sorting startcall to get min and max startcall
	p.sortStartCall(closingStruct)

	// fmt.Println("Success => [ Rejected", p.totalReject, ", Dup", p.totalDup, ", Undup", p.totalUndup, "records ]")
	fmt.Printf("Success => [ Rejected %d, Dup %d, Undup %d records, Dup Dur %d seconds, Undup Dur %d seconds ]\n", p.totalReject, p.totalDup, p.totalUndup, p.totalDupDur, p.totalUndupDur)

	fmt.Println("  > [ Min Startcall :", closingStruct.MinStartcall, ", Max Startcall :", closingStruct.MaxStartcall, "]")

	closingStruct.ProcessStatusID = "7"

	return nil
}

func (p *OCS) sortStartCall(closingStruct *model.ClosingStruct) {
	// Sorting startcall to get min and max startcall
	var keys []int
	for k := range p.mapStartCall {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	closingStruct.MinStartcall, closingStruct.MaxStartcall = strconv.Itoa(keys[0]), strconv.Itoa(keys[len(keys)-1])
}

func (p *OCS) lineProcessing(dbconn *leveldb.DB, sc *bufio.Scanner, writerDUP, writerReject, writerUndup *bufio.Writer) {
	var keydup string
	var artoleransi []string

	columns := strings.Split(strings.Replace(sc.Text(), "+", "", -1), "|")

	operatorID := columns[p.urutanOprID]

	if p.mapCount[operatorID] == nil {
		p.mapCount[operatorID] = map[string]int{}
	}

	for _, vParamCompare := range p.CheckDupConf.ParameterCompare {
		urutan, _ := strconv.Atoi(vParamCompare)
		keydup += columns[urutan]
	}

	for _, vParamCompare := range p.CheckDupConf.ParameterToleransi {
		urutan, _ := strconv.Atoi(vParamCompare)
		artoleransi = append(artoleransi, columns[urutan])
	}
	toleransi := strings.Join(artoleransi, "-")

	data, _ := dbconn.Get([]byte(keydup+"-"+toleransi), nil)
	dataCombine, _ := p.DBConn.Get([]byte(keydup+"-"+toleransi), nil)
	dur, _ := strconv.Atoi(columns[p.urutanDurasi])
	if data == nil && dataCombine == nil {
		dbconn.Put([]byte(keydup+"-"+toleransi), sc.Bytes(), nil)
		fmt.Fprintln(writerUndup, sc.Text())
		stcall, _ := strconv.Atoi(columns[p.urutanStartcall])
		p.mapStartCall[stcall] = 1
		p.totalUndup++
		p.totalUndupDur += dur
		p.mapCount[operatorID]["undup_records"]++
		p.mapCount[operatorID]["undup_dur"] += dur
	} else {
		fmt.Fprintln(writerDUP, sc.Text())
		p.totalDup++
		p.totalDupDur += dur
		p.mapCount[operatorID]["dup_records"]++
		p.mapCount[operatorID]["dup_dur"] += dur
	}
}
