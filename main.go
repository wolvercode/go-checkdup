package main

import (
	"fmt"
	"os"

	"bitbucket.org/billing/go-checkdup/app"
	"bitbucket.org/billing/go-checkdup/handler"
	"bitbucket.org/billing/go-checkdup/model"
)

func main() {
	for {
		app.Initialize()

		flowControl()

		app.Sleeping()
	}
}

func flowControl() error {
	fmt.Printf("[ Getting Jobs From Database ] : ")
	handlerProcess := handler.Process{}
	listProcess, err := handlerProcess.GetProcess()
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	if listProcess == nil {
		fmt.Println("No job found in database")
		return nil
	}

	fmt.Println(len(listProcess), "Jobs")

	fmt.Printf("[ Getting Data Structure From Database ] : ")
	dataStructure, err := handlerProcess.GetDataStructure()
	if err != nil {
		fmt.Println("Error =>", err.Error())
		return err
	}

	fmt.Println("Success")

	fmt.Printf("[ Getting Check Duplicate Configuration From Database ] : ")
	checkDupConf, err := handlerProcess.GetCheckDupConf()
	if err != nil {
		fmt.Println("Error =>", err.Error())
		return err
	}

	if checkDupConf.Conf == nil {
		fmt.Println("Failed, no Check Duplicate Configuration found in database")
		return nil
	}

	fmt.Println("Success")

	for _, v := range listProcess {
		fmt.Println("[ Processing ] : { Process ID :", v.ProcessID, "}")
		handlerProcess.UpdateStatus(v.ProcessID, "4")

		closingStruct := model.ClosingStruct{
			ProcessConf:     &v,
			ProcessStatusID: "4",
			MinStartcall:    "0",
			MaxStartcall:    "0",
		}

		switch v.DataTypeID {
		case 1:
			processOCS(&v, &handlerProcess, checkDupConf.Conf["OCS"], &dataStructure, &closingStruct)
			handlerProcess.CloseProcess(&closingStruct)
		case 2:
			processTAPIN(&v, &handlerProcess, checkDupConf.Conf["TAPIN"], &dataStructure, &closingStruct)
			handlerProcess.CloseProcess(&closingStruct)
		}
	}

	return nil
}

func processOCS(v *model.ProcessCheckdup, handlerProcess *handler.Process, checkDupConf model.CheckDupParameter, dataStructure *model.DataStructure, closingStruct *model.ClosingStruct) {
	handlerOCS := handler.OCS{ListProcess: v, CheckDupConf: checkDupConf, DataStructure: dataStructure}

	countInput, err := handlerOCS.LogStartcall()
	if err != nil {
		fmt.Println("  > Error =>", err.Error())
		closingStruct.ProcessStatusID = "5"
		closingStruct.ErrorMessage = err.Error()
		return
	}
	if countInput == 0 {
		fmt.Println("  > File has no records")
		closingStruct.ProcessStatusID = "5"
		closingStruct.ErrorMessage = err.Error()
		return
	}

	listPathDB, err := handlerProcess.GetSlicedProcess(handlerOCS.MinStartcall, handlerOCS.MaxStartcall, "1", v.MoMt, v.PostPre)
	if err != nil {
		fmt.Println(err.Error())
		closingStruct.ProcessStatusID = "5"
		closingStruct.ErrorMessage = err.Error()
		return
	}

	if listPathDB == nil {
		fmt.Println("  > [ No Path DB found in database ]")
	}

	currentDBPath := app.Appl.Configs.Main.ProcessDir + v.ProcessID

	handlerCommon := handler.CommonHandler{}
	err = handlerCommon.CombineDB(&listPathDB, currentDBPath)
	if err != nil {
		closingStruct.ProcessStatusID = "5"
		closingStruct.ErrorMessage = err.Error()
		return
	}

	err = handlerOCS.ProcessData(closingStruct, currentDBPath)
	if err != nil {
		closingStruct.ProcessStatusID = "5"
		closingStruct.ErrorMessage = err.Error()
		return
	}

	defer handlerOCS.DBConn.Close()

	os.RemoveAll(currentDBPath)

	return
}

func processTAPIN(v *model.ProcessCheckdup, handlerProcess *handler.Process, checkDupConf model.CheckDupParameter, dataStructure *model.DataStructure, closingStruct *model.ClosingStruct) {
	handlerTapin := handler.Tapin{ListProcess: v, CheckDupConf: checkDupConf, DataStructure: dataStructure}

	countInput, err := handlerTapin.LogStartcall()
	if err != nil {
		fmt.Println("  > Error =>", err.Error())
		closingStruct.ProcessStatusID = "5"
		closingStruct.ErrorMessage = err.Error()
		return
	}
	if countInput == 0 {
		fmt.Println("  > File has no records")
		closingStruct.ProcessStatusID = "5"
		closingStruct.ErrorMessage = err.Error()
		return
	}

	listPathDB, err := handlerProcess.GetSlicedProcess(handlerTapin.MinStartcall, handlerTapin.MaxStartcall, "2", v.MoMt, v.PostPre)
	if err != nil {
		fmt.Println(err.Error())
		closingStruct.ProcessStatusID = "5"
		closingStruct.ErrorMessage = err.Error()
		return
	}

	if listPathDB == nil {
		fmt.Println("  > [ No Path DB found in database ]")
	}

	currentDBPath := app.Appl.Configs.Main.ProcessDir + v.ProcessID

	handlerCommon := handler.CommonHandler{}
	err = handlerCommon.CombineDB(&listPathDB, currentDBPath)
	if err != nil {
		closingStruct.ProcessStatusID = "5"
		closingStruct.ErrorMessage = err.Error()
		return
	}

	err = handlerTapin.ProcessData(closingStruct, currentDBPath)
	if err != nil {
		closingStruct.ProcessStatusID = "5"
		closingStruct.ErrorMessage = err.Error()
		return
	}

	defer handlerTapin.DBConn.Close()

	os.RemoveAll(currentDBPath)

	return
}
