package model

// DataStructure :
type DataStructure struct {
	TypeData map[string]DataStructureParameter `json:"data_type"`
}

// DataStructureParameter :
type DataStructureParameter struct {
	Parameter map[string]DataStructureProperties `json:"parameter"`
}

// DataStructureProperties :
type DataStructureProperties struct {
	Urutan     int `json:"urutan"`
	SortUrutan int `json:"sort_urutan"`
}

// CheckDupConf :
type CheckDupConf struct {
	Conf map[string]CheckDupParameter `json:"conf"`
}

// CheckDupParameter :
type CheckDupParameter struct {
	Type               string   `json:"type"`
	ParameterContains  string   `json:"parameter_contains"`
	ParameterCompare   []string `json:"parameter_compare"`
	ParameterToleransi []string `json:"parameter_toleransi"`
}

// SlicedProcess :
type SlicedProcess struct {
	PathDB string `json:"path_db"`
}

// ClosingStruct :
type ClosingStruct struct {
	ProcessConf         *ProcessCheckdup          `json:"process_conf"`
	ProcessStatusID     string                    `json:"processstatus_id"`
	TotalInput          int                       `json:"total_input"`
	TotalReject         int                       `json:"total_reject"`
	TotalDuplicate      int                       `json:"total_duplicate"`
	TotalUnduplicate    int                       `json:"total_unduplicate"`
	DuplicateDuration   int                       `json:"duplicate_duration"`
	UnduplicateDuration int                       `json:"unduplicate_duration"`
	MinStartcall        string                    `json:"min_startcall"`
	MaxStartcall        string                    `json:"max_startcall"`
	ErrorMessage        string                    `json:"error_message"`
	MapCount            map[string]map[string]int `json:"map_count"`
}
