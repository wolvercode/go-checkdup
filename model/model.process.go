package model

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"bitbucket.org/billing/go-checkdup/app"
)

// ProcessCheckdup :
type ProcessCheckdup struct {
	ProcessID      string `json:"process_id"`
	ProcesstypeID  string `json:"processtype_id"`
	BatchID        string `json:"batch_id"`
	DataTypeID     int    `json:"data_type_id"`
	Periode        string `json:"periode"`
	DayPeriode     string `json:"day"`
	MoMt           string `json:"mo_mt"`
	PostPre        string `json:"post_pre"`
	CdrID          int    `json:"cdr_id"`
	PathCdr        string `json:"path_cdr"`
	FilenameCdr    string `json:"filename_cdr"`
	DupID          int    `json:"dup_id"`
	PathDup        string `json:"path_dup"`
	FilenameDup    string `json:"filename_dup"`
	DBID           int    `json:"db_id"`
	PathDB         string `json:"path_db"`
	RejectID       int    `json:"reject_id"`
	PathReject     string `json:"path_reject"`
	FilenameReject string `json:"filename_reject"`
	UndupID        int    `json:"undup_id"`
	PathUndup      string `json:"path_undup"`
	FilenameUndup  string `json:"filename_undup"`
}

// GetProcess :
func (p *ProcessCheckdup) GetProcess() ([]ProcessCheckdup, error) {
	var listProcess []ProcessCheckdup

	response, err := http.Get(app.Appl.DBAPIURL + "process/checkdup/getProcess")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listProcess)

	return listProcess, nil
}

// UpdateStatus :
func (p *ProcessCheckdup) UpdateStatus(processID, processStatusID string) error {
	client := &http.Client{}
	response, err := http.NewRequest(http.MethodPut, app.Appl.DBAPIURL+"process/updateStatus/"+processID+"/"+processStatusID, bytes.NewBuffer(nil))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	_, err = client.Do(response)
	if err != nil {
		// handle error
		log.Fatal(err)
	}
	defer io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}

// GetCheckDupConf :
func (p *ProcessCheckdup) GetCheckDupConf() (CheckDupConf, error) {
	var checkDupConf CheckDupConf

	response, err := http.Get(app.Appl.DBAPIURL + "process/checkdup/getCheckDupConf")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return CheckDupConf{}, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&checkDupConf)

	return checkDupConf, nil
}

// GetDataStructure :
func (p *ProcessCheckdup) GetDataStructure() (DataStructure, error) {
	var dataStructure DataStructure

	response, err := http.Get(app.Appl.DBAPIURL + "configuration/getDataStructure")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return dataStructure, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&dataStructure)

	return dataStructure, nil
}

// GetSlicedProcess :
func (p *ProcessCheckdup) GetSlicedProcess(minStartcall, maxStartcall, dataTypeID, moMt, postPre string) ([]SlicedProcess, error) {
	var listPathDB []SlicedProcess

	response, err := http.Get(app.Appl.DBAPIURL + "process/getSlicedProcess/" + minStartcall + "/" + maxStartcall + "/" + dataTypeID + "/" + moMt + "/" + postPre)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listPathDB)

	return listPathDB, nil
}

// CloseProcess :
func (p *ProcessCheckdup) CloseProcess(closeStruct *ClosingStruct) error {
	jsonValue, _ := json.Marshal(closeStruct)
	response, err := http.Post(app.Appl.DBAPIURL+"process/checkdup/closeProcess/", "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}
